import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
//引入element ui
import Element from 'element-ui'
import "element-ui/lib/theme-chalk/index.css"
//引入axios
import axios from 'axios'
//全局请求拦截导入
import "./axios"
//引入 md编辑器
import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'
//引入权限判断
import './permission'

Vue.use(Element)//全局使用
Vue.use(mavonEditor)

Vue.config.productionTip = false
Vue.prototype.$axios = axios //

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
