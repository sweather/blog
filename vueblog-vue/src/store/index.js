import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    toekn:'',
    userInfo: JSON.parse(sessionStorage.getItem("userInfo"))
  },
  mutations: {
    //set方法
    SET_TOKEN:(state,toekn)=>{
        state.toekn=toekn
        //存入本地，关了浏览器也行
        localStorage.setItem("token",toekn)
    },
    SET_USERINFO:(state,userInfo)=>{
      state.userInfo=userInfo
      //存入本地，关了浏览器也行
      sessionStorage.setItem("userInfo",JSON.stringify(userInfo))
  },
  REMOVE_INFO:(state)=>{
    state.toekn=''
    state.userInfo={}
    //存入本地，关了浏览器也有
    localStorage.setItem("token",'')
    sessionStorage.setItem("userInfo",JSON.stringify({}))
}
  },
  getters:{
    //get方法
    getUser:state =>{
      return state.userInfo
    }
  },
  actions: {
    
  },
  modules: {
  }
})
