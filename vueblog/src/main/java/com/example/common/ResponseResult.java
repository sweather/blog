package com.example.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseResult implements Serializable {
    private int code;//200是正常，其他为异常
    private String msg;
    private Object data;

    public static ResponseResult success(String msg,Object data){
        return new ResponseResult(200,msg,data);
    }

    public static ResponseResult success(Object data){
        return new ResponseResult(200,"操作成功",data);
    }
    public static ResponseResult fail(String msg,Object data){
        return new ResponseResult(-1,msg,data);
    }
    public static ResponseResult fail(int code,String msg,Object data){
        return new ResponseResult(code,msg,data);
    }

    public static ResponseResult fail(String msg){
        return new ResponseResult(-1,msg,null);
    }

}
