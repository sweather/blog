package com.example.common.exception;

import com.example.common.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.ShiroException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常捕获，异步
 */
@Slf4j
@RestControllerAdvice
public class GlobalException {


    /**
     * 捕捉shiro异常
     * @param e
     * @return
     */
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(value =ShiroException.class )
    public ResponseResult handler(ShiroException e){
        log.error("运行时异常：--------");
        return ResponseResult.fail(401,e.getMessage(),null);
    }

    /**
     *  捕捉实体校验异常
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = MethodArgumentNotValidException.class )
    public ResponseResult handler(MethodArgumentNotValidException e){
        log.error("实体校验异常：--------");
        return ResponseResult.fail(e.getMessage());
    }

    /**
     *  捕捉断言异常
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = IllegalArgumentException.class )
    public ResponseResult handler(IllegalArgumentException e){
        log.error("断言异常：--------");
        return ResponseResult.fail(e.getMessage());
    }

    /**
     * 全局大异常
     * @param e
     * @return
     */
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(value =RuntimeException.class )
    public ResponseResult handler(RuntimeException e){
        log.error("运行时异常：--------");
        return ResponseResult.fail(401,e.getMessage(),null);
    }
}
