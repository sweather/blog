package com.example.controller;


import com.example.common.ResponseResult;
import com.example.service.UserService;
import com.example.shiro.AccountProfile;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Qiu
 * @since 2020-06-30
 */
@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;


    @RequiresAuthentication//授权
    @GetMapping()
    public ResponseResult index(){
        Object principal = SecurityUtils.getSubject().getPrincipal();
        AccountProfile profile=(AccountProfile) principal;
        System.out.println("subject:"+profile.getId());

       return ResponseResult.success(userService.getById(1l));
    }

}
