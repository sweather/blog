package com.example.mapper;

import com.example.common.ResponseResult;
import com.example.entity.Blog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Qiu
 * @since 2020-06-30
 */
public interface BlogMapper extends BaseMapper<Blog> {


}
