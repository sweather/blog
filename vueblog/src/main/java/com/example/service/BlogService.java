package com.example.service;

import com.example.entity.Blog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Qiu
 * @since 2020-06-30
 */
public interface BlogService extends IService<Blog> {

}
